Django TU Authentication
========================

Data Dict
============
| Field name | type | description |
|------------|------|-------------|
| birthday | varchar(100) | Date of Birth |
| email | varchar(255) | Gennext email |
| username | varchar(100) | - |
| studnet_id | varchar(100) | not used |
| isactive | boolean | active status |
| firstname | varchar(100) | - |
| lastname | varchar(100) | - |
| fullname | varchar(100) | - |
| staffmail | varchar(100) | Tu staff email |
| type | varchar(100) | Type of user: ปกติ, พนักงานมหาวิทยาลัย, ข้าราชการ |
| tumail | varchar(100) | Tu email |
| department | varchar(100) | Faculty's department |
| company | varchar(100) | Faculty |
| role | varchar(1) | 1 for Student, 2 for Staff, 3 for Instructor, 4 for Gennext |


Requirements
============
- python (2.7, 3.5)
- django (1.11, 2.0)
- social-app-django (2.1.0)

Installation
============
```
pip install django-tuauth
```

Usage
=====
### Prerequisite
Register account in https://reg.gennext.tu.ac.th/

Register application in https://api.tu.ac.th/o/applications/register/
> note: Callback URL must be same with decarelation in urls.py
> in this example use http://127.0.0.1/oauth/complete/tu/

### in setting.py 
```python
INSTALLED_APPS = [
    ...
    'social_django',
    'tuauth',
    ...
]
```
add authentication backend in setting.py
```python
AUTHENTICATION_BACKENDS = (
    ...
    'tuauth.backend.TUOAuth2',
    ...
)
```
set client id and client secret in setting.py
```python
SOCIAL_AUTH_TU_KEY = '<client_id>'
SOCIAL_AUTH_TU_SECRET = '<client_secret>'
```

Sample SOCIAL_AUTH_PIPELINE
```python
SOCIAL_AUTH_PIPELINE = [ 
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    'social_core.pipeline.social_auth.associate_by_email',
]
```
> See more detail about **social-app-django** in (https://github.com/python-social-auth/social-app-django)

### in urls.py
```
urlpatterns = [
    ...
    path('oauth/', include('social_django.urls', namespace='social')), # in django2
    ...
]
```

### in template
```
    ...
        <a href="{% url 'social:begin' 'tu' %}">Login with TU</a>
    ...
```
